# jQuery Demo

<img src="https://fivethirtyeight.com/wp-content/uploads/2015/10/2_kappadeseno.jpg?quality=100&strip=all" alt="banner" width="450"/>

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)
[![pages](https://img.shields.io/static/v1?logo=gitlab&message=view&label=pages&color=e24329)](https://noroff-accelerate.gitlab.io/javascript/jquery-skeleton)

> jQUery assignment with Blogpost API from https://jsonplaceholder.typicode.com/

## Table of Contents

- [Install](#install)
- [Develop](#develop)
- [Build](#build)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)
- [Contribution](#Contribution)

## Install

```bash
npm install
```

## Develop

Start the Webpack development server.

```bash
npm start
```

## Build

Create a minified, production build of the project for publication.

```bash
npm run build
```

## Maintainers

[Philip Aubert (@pauberexperis)](https://gitlab.com/pauberexperis)

[Christopher Toussi (@ctuss)](https://gitlab.com/ctuss)

## Contributing

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

Experis © 2020 Noroff Accelerate AS

## Contribution

In order to maintain our website donations are much appreciated, but not necessary.
Thank you for your support.